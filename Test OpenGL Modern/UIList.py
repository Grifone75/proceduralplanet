

class UIList:
	"""
	Unique Index List is a special list type which guarantees that a item will
	always have the same index once inserted, thus allowing faster access to it
	this is to optimize performance during drawing
	"""
	
	def __init__(self):
		self.list = []
		self.freepos = []
		pass
	
	def add(self,element):
		if not self.freepos:
			self.list.append(element)
			return len(self.list)-1
		else:
			free = self.freepos.pop(0)
			self.list[free] = element
			return free

	def remove(self,index):
		self.list[index] = None
		self.freepos.append(index)

	def printlist(self):
		for el in self.list:
			print el



class ExtUIList(UIList):
	""" 
	an extention of UIList dedicated to vbo pool implementation
	offering a staging state, to add element not yet actives and to reserve element for deletion
	"""

	def __init__(self):
		UIList.__init__(self)
		self.actives = []
		self.to_add = []
		self.to_delete = []

	def add(self,element):
		pos = UIList.add(self,element)
		self.actives.append(pos)
		return pos
	
	def remove(self,index_rem,function_el):
		# remove it from list
		function_el(self.list(index_rem))
		UIList.remove(self,index_rem)
		#search and remove from support listes
		try: 
			self.actives.remove(index_rem)
		except:
			pass
		try:
			self.to_add.remove(index_rem)
		except:
			pass
		try:
			self.to_delete.remove(index_rem)
		except:
			pass
		
	
	def add_in_staging(self,element):
		pos = UIList.add(self,element)
		self.to_add.append(pos)
		return pos
		
	def remove_in_staging(self,index_rem):
		self.to_delete.append(index_rem)
	
	def apply_changes(self,function_el):
		for el in self.to_add:
			self.actives.append(el)
		self.to_add = []
		for el in self.to_delete:
			function_el(self.list(el))
			self.remove(el)
		self.to_delete = []
	
	def __iter__(self):
		for el in self.actives:
			yield self.list[el]
		
	def get(self,index):
		return self.list(index)
		