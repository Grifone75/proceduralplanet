import OpenGL 
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from OpenGL.arrays import vbo 
from UIList import ExtUIList


class VboPool():
    """ 
    class to manage a pool of vbos and ibos
    linked with the planet nodes
    each element is linked to an indexto allow the 
    node to retrieve and to a active/unactive status
    used in the iteration (only active ones are iterated)
    the structure in the list is a dictionary:
    index
    vbo
    ibo
    """

    
    def __init__(self):
        self.list = ExtUIList()

         
    def __iter__(self):
        for el in self.list:
            yield el
    
        
    def next(self):
        return self.list.next()
    
    
    def add_vbo_ibo(self,vbodata,ibodata,active = False):
        #add a new element
        element = {'vbo':vbo.VBO(data=vbodata, usage=GL_DYNAMIC_DRAW, target=GL_ARRAY_BUFFER),
                   'ibo':vbo.VBO(data=ibodata, usage=GL_DYNAMIC_DRAW, target=GL_ELEMENT_ARRAY_BUFFER)}
        if active:
            return self.list.add(element)
        else:
            return self.list.add_in_staging(element)
    
    def remove_vbo_ibo(self,index,immediate = False):
        if immediate:
            self.list.remove(index,remover)
        else:
            self.list.remove_in_staging(index)
            
    def apply_changes(self):
        self.list.apply_changes(remover)

def remover(element):
    #replace with code to delete the vbos
    print element['vbo']
    print element['ibo']


